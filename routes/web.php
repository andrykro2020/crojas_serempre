<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Auth::routes();

Route::post('/passwordReset', 'UserController@passwordReset')->name('passwordReset');
Route::get('/passwordResetView', 'UserController@passwordResetView')->name('passwordResetView');
Route::get('/userHome', 'UserController@userHome')->name('userHome');

Route::group(['middleware' => 'auth'], function () {

    Route::resource('user', 'UserController', ['except' => ['show']]);

    Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
    Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
    Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);

    Route::post('filter', 'ClientController@filter')->name('filter');
    Route::resource('client', 'ClientController', ['except' => ['show']]);

    Route::resource('city', 'CityController', ['except' => ['show']]);
});



