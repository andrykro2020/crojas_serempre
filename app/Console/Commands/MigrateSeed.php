<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Exception;

class MigrateSeed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrateSeed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ejecución de migraciones y seeder en un solo comando';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * Generación de comando para ejecución de migraciones y poblado de datos por medio de seeders.
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->info('Se inician las migraciones' );
            \Artisan::call(' migrate --seed');
            $this->info('Se ejecutaron las migraciones de forma exitosa');

        } catch (Exception $e) {
            $this->info('Se presento un problema en las migraciones: ' . $e->getMessage());
        }
    }
}
