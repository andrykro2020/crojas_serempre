<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = [
        'id', 'cod', 'name', 'city_id'
    ];

    public function city()
    {
        return $this->belongsTo(City::class);
    }
}
