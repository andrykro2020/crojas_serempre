<?php

namespace App\Http\Controllers;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     * Llamad de autenticación
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     * Llamado a vista inicial de dashboard
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('dashboard');
    }
}
