<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Exception;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     * Llamado de todas los clientes, paginadas en grupos de a 5. Adicional cuenta con el filtro de ciudades
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $clients = Client::with('city')
        ->when($request->city_id, function ($query) use ($request) {
            return $query->where('city_id', $request->city_id);
        })
        ->paginate(5);
        $cities = City::all();
        return view('clients.index', ['clients' => $clients, 'cities' => $cities]);
    }

    /**
     * Show the form for creating a new resource.
     *Llamado a la vista del formulario de creación de un cliente
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cities = City::all();
        return view('clients.create', ['cities' => $cities]);
    }

    /**
     * Store a newly created resource in storage.
     * Método de creación de un cliente: parametros cod,name y city_id obligatorios.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cod' => 'required',
            'name' => 'required',
            'city_id' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return ($validator->errors());
        }
        try {
            $client = new Client;
            $client = $client->create($request->all());
        } catch (Exception $e) {
            return redirect()->route('client.index')->withStatus(__($e->getMessage()));
        }
        return redirect()->route('client.index')->withStatus(__('Cliente creado con éxito.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *Llamado a la vista del formulario de actualización de un cliente
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        $cities = City::all();
        return view('clients.edit', ['client' => $client, 'cities' => $cities]);
    }

    /**
     * Update the specified resource in storage.
     * Método de actualización de un cliente: parametros cod,name y city_id obligatorios.
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        $validator = Validator::make($request->all(), [
            'cod' => 'required',
            'name' => 'required',
            'city_id' => 'required|numeric',
            'updated_at' => 'nullable|date'
        ]);

        if ($validator->fails()) {
            return ($validator->errors());
        }

        try {
            $client->update($request->all());
        } catch (Exception $e) {
            return redirect()->route('client.index')->withStatus(__($e->getMessage()));
        }
        return redirect()->route('client.index')->withStatus(__($e->getMessage()));
    }

    /**
     * Remove the specified resource from storage.
     * Método de eliminación de un cliente
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        $client->delete();
        return redirect()->route('client.index')->withStatus(__('Cliente eliminado con éxito.'));
    }
}
