<?php

namespace App\Http\Controllers;

use App\Models\City;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Exception;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     * Llamado de todas las ciudades, paginadas en grupos de a 5.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cities = City::paginate(5);
        return view('cities.index', ['cities' => $cities]);
    }

    /**
     * Show the form for creating a new resource.
     *Llamado a la vista del formulario de creación de una ciudad
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cities.create');
    }

    /**
     * Store a newly created resource in storage.
     * Método de creación de una ciudad: parametros cod y name obligatorios.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cod' => 'required',
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return ($validator->errors());
        }

        try {
            $city = new City;
            $city = $city->create($request->all());
        } catch (Exception $e) {
            return redirect()->route('city.index')->withStatus(__($e->getMessage()));
        }
        return redirect()->route('city.index')->withStatus(__('Ciudad creada con éxito.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\City  $city
     * @return \Illuminate\Http\Response
     */
    public function show(City $city)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * Método para el llamado de un formulario de edición de una ciudad
     * @param  \App\Models\City  $city
     * @return \Illuminate\Http\Response
     */
    public function edit(City $city)
    {
        return view('cities.edit', ['city' => $city]);
    }

    /**
     * Update the specified resource in storage.
     * Método de actualización de una ciudad , con parametros cod y name obligatorios.
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\City  $city
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, City $city)
    {
        $validator = Validator::make($request->all(), [
            'cod' => 'required',
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return ($validator->errors());
        }

        try {
            $city->update($request->all());
        } catch (Exception $e) {
            return redirect()->route('city.index')->withStatus(__($e->getMessage()));
        }
        return redirect()->route('city.index')->withStatus(__('Ciudad Actualizada con éxito.'));
    }

    /**
     * Remove the specified resource from storage.
     * Método de eliminación de una ciudad
     * @param  \App\Models\City  $city
     * @return \Illuminate\Http\Response
     */
    public function destroy(City $city)
    {
        $city->delete();
        return redirect()->route('city.index')->withStatus(__('Ciudad eliminada con éxito.'));
    }
}
