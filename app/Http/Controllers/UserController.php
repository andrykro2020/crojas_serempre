<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Requests\UserRequest;
use App\Mail\userCreate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Exception;

class UserController extends Controller
{
    /**
     * Display a listing of the users
     * Llamado de todas los usuarios, paginadas en grupos de a 5.
     * @param  \App\Models\User  $model
     * @return \Illuminate\View\View
     */
    public function index(User $model)
    {
        return view('users.index', ['users' => $model->paginate(5)]);
    }

    /**
     * Show the form for creating a new user
     *Llamado a la vista del formulario de creación de un usuario
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created user in storage
     * Método de creación de un usuario.
     * @param  \App\Http\Requests\UserRequest  $request
     * @param  \App\Models\User  $model
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(UserRequest $request, User $user)
    {
        try{
            $user = $user->create($request->merge(['password' => Hash::make($request->get('password'))])->all());
            $user->sendPasswordResetNotification(app('auth.password.broker')->createToken($user));

        } catch (Exception $e) {
            return redirect()->route('user.index')->withStatus(__($e->getMessage()));
        }
        return redirect()->route('user.index')->withStatus(__('Usuario creado con éxito.'));
    }

    /**
     * Show the form for editing the specified user
     *Llamado a la vista del formulario de actualización de un usuario
     * @param  \App\Models\User  $user
     * @return \Illuminate\View\View
     */
    public function edit(User $user)
    {
        return view('users.edit', compact('user'));
    }

    /**
     * Update the specified user in storage
     * Método de actualización de un usuario.
     * @param  \App\Http\Requests\UserRequest  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UserRequest $request, User  $user)
    {
        try{
            $user->update(
                $request->merge(['password' => Hash::make($request->get('password'))])
                    ->except([$request->get('password') ? '' : 'password']
            ));

        } catch (Exception $e) {
            return redirect()->route('user.index')->withStatus(__($e->getMessage()));
        }
        return redirect()->route('user.index')->withStatus(__('Usuario actualizado con éxito.'));
    }

    /**
     * Remove the specified user from storage
     * Método de eliminación de un usuario
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(User  $user)
    {
        $user->delete();
        return redirect()->route('user.index')->withStatus(__('Usuario eliminado con éxito.'));
    }

    /**
     * Método de cambio de contraseña de un usuario
     */
    public function passwordReset(Request $request){
        try {
            $user = User::where('email',$request->input('email'))->first();
            if(!$user){
                return redirect()->route('userHome')->withStatus(__('El usuario no se encuentra registrado en el sistema, por favor validar el usaurio'));
            }
            $user->update(['password' => Hash::make($request->input('password'))]);
        } catch (Exception $e) {
            return redirect()->route('userHome')->withStatus(__($e->getMessage()));
        }
        return redirect()->route('userHome')->withStatus(__('El cambio de contraseña se realizó de manera exitosa'));
    }

    /**
     * Metodo de re-direccón a la pagina principal
     */
    public function userHome(Request $request){
        return view('welcome');
    }

    /**
     * Metodo de re-direccón a la pagina de cambio de contraseña
     */
    public function passwordResetView(Request $request){
        return view('users.resetPassword');
    }

}
