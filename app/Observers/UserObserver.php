<?php

namespace App\Observers;

use App\User;
use Exception;
use Illuminate\Support\Facades\Artisan;

class UserObserver
{
    /**
     * Handle the user "created" event.
     * Observador activo para la determinación de la creación de un nuevo usuario, inicializa el trabajo del encolamiento, para lograr el envío de los correos correctamente.
     * @param  \App\User  $user
     * @return void
     */
    public function created(User $user)
    {
        /*try {
            Artisan::call(' queue:work');
        } catch (Exception $e) {

        }*/
    }

    /**
     * Handle the user "updated" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function updated(User $user)
    {

    }

    /**
     * Handle the user "deleted" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function deleted(User $user)
    {
        //
    }

    /**
     * Handle the user "restored" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function restored(User $user)
    {
        //
    }

    /**
     * Handle the user "force deleted" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function forceDeleted(User $user)
    {
        //
    }
}
