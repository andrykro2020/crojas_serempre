@extends('layouts.app', ['title' => __('Crean Cliente')])

@section('content')
    @include('users.partials.header', ['title' => __('Creación de clientes')])
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Lista de clientes') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('client.index') }}" class="btn btn-sm btn-primary">{{ __('Volver a la lista') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('client.store') }}" autocomplete="off">
                            @csrf

                            <h6 class="heading-small text-muted mb-4">{{ __('Informacion del cliente') }}</h6>
                            <div class="pl-lg-4">
                                <div class="form-group{{ $errors->has('cod') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-name">{{ __('Código') }}</label>
                                    <input type="text" name="cod" id="input-name" class="form-control form-control-alternative{{ $errors->has('cod') ? ' is-invalid' : '' }}" placeholder="{{ __('Código') }}" value="{{ old('cod') }}" required autofocus>

                                    @if ($errors->has('cod'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('cod') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-name">{{ __('Nombre') }}</label>
                                    <input type="text" name="name" id="input-name" class="form-control form-control-alternative{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="{{ __('Nombre') }}" value="{{ old('name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('value') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-value">{{ __('Ciudad') }}</label>
                                    <select name="city_id" id="city_id" required class="form-control">
                                        <option value="">Seleccione una ciudad</option>
                                        @foreach ($cities as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Guardar') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footers.auth')
    </div>
@endsection
