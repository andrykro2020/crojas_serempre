<div class="row align-items-center">
    <form method="get" action="{{ route('client.index') }}" autocomplete="off">
        <div class="col-10 text-left">
            <select name="city_id" id="city_id" required class="form-control" class="small">
                <option value="">Seleccione una ciudad</option>
                @foreach ($cities as $item)
                <option value="{{$item->id}}">{{$item->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-2 text-left">
            <button type="submit" class="btn btn-success mt-4">{{ __('Filtrar') }}</button>
        </div>
    </form>
</div>
