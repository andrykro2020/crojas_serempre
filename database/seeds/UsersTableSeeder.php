<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Poblado de datos de usuario administrador
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Administrador',
            'email' => 'admin@serempre.com',
            'email_verified_at' => now(),
            'password' => Hash::make('admin102030'),
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
