<?php

use App\Models\City;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class CitySeeder extends Seeder
{
    /**
     * Poblado de datos de ciudades.
     *
     * @return void
     */
    public function run()
    {
        $cities = [
            [
                'cod' => '001',
                'name' => 'Barranquilla',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'cod' => '002',
                'name' => 'Bogotá',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'cod' => '003',
                'name' => 'Cartagena',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'cod' => '004',
                'name' => 'Cali',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'cod' => '005',
                'name' => 'Medellin',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'cod' => '006',
                'name' => 'Rioacha',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ];

        $citiesInsert = new City;
        $citiesInsert->insert($cities);
    }
}
